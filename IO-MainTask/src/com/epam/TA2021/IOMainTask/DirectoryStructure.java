package com.epam.TA2021.IOMainTask;

import java.io.*;

public class DirectoryStructure {
    private static final String INDENT = "---/";

    public static void main(String[] args) {

        int countOfFiles;
        int countOfFolders;
        double averageLengthOfFileNames;

        File sourceDirectory = new File("G:\\Виктор\\MUSIC");
        String destinationFile = ".\\src\\com\\epam\\TA2021\\IOMainTask\\Structure.txt";

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(destinationFile))) {
            if (sourceDirectory.exists() && sourceDirectory.isDirectory()) {
                writeStructureDirectory(sourceDirectory, bufferedWriter, 0);
            }
        } catch (IOException x) {
            x.printStackTrace();
        }

        countOfFolders = countOfFolders(destinationFile);
        countOfFiles = countOfFiles(destinationFile);
        averageLengthOfFileNames = averageLengthOfFileNames(destinationFile, countOfFiles);

        System.out.println("Folders count - " + countOfFolders);
        System.out.println("Files count - " + countOfFiles);
        System.out.println("Average amount of files in folders - " + countOfFiles / (double) countOfFolders);
        System.out.println("Average length of file names - " + averageLengthOfFileNames);
    }

    private static void writeStructureDirectory(File directory, BufferedWriter bufferedWriter, int level) {
        try {
            File[] files = directory.listFiles();
            if (level == 0) bufferedWriter.write("Source Folder: " + directory.getName() + "\n");
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        for (int i = 0; i < level; i++) {
                            bufferedWriter.write(INDENT);
                        }
                        bufferedWriter.write(INDENT + file.getName() + "\n");
                        writeStructureDirectory(file, bufferedWriter, level + 1);
                    } else {
                        for (int i = 0; i < level; i++) {
                            bufferedWriter.write("   ");
                        }
                        bufferedWriter.write(file.getName() + "\n");
                    }
                }
            }
        } catch (IOException x) {
            x.printStackTrace();
        }
    }

    private static int countOfFolders(String fileName) {
        int countOfFolders = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while (( line = bufferedReader.readLine() ) != null) {
                if (line.contains(INDENT)) {
                    countOfFolders++;
                }
            }
        } catch (IOException x) {
            x.printStackTrace();
        }
        return countOfFolders;
    }

    private static int countOfFiles(String fileName) {
        int countOfFiles = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while (( line = bufferedReader.readLine() ) != null) {
                if (line.contains(".")) {
                    countOfFiles++;
                }
            }
        } catch (IOException x) {
            x.printStackTrace();
        }
        return countOfFiles;
    }

    private static double averageLengthOfFileNames(String fileName, int countOfFiles) {
        int count = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while (( line = bufferedReader.readLine() ) != null) {
                if (line.contains(".")) {
                    count = count + line.trim().length();
                }
            }
        } catch (IOException x) {
            x.printStackTrace();
        }
        return count / (double) countOfFiles;
    }
}

